# [ideme.fr](https://ideme.fr)

Homepage Ideme

## Lancement en local

Installation
```sh
npm i
```

Lancement en mode dev
```sh
npm run serve
```

- [site local](http://localhost:3000)
- [pattern library local](http://localhost:3000/documentation.html)

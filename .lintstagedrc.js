module.exports = {
  'src/**/*.{css,scss}': ['stylelint --fix'],
  'src/**/*.pug': ['pug-lint']
};
